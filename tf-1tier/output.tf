output "vpc_id" {
    value = "${aws_vpc.default.id}"
}

output "subnet_ids" {
    value = ["${aws_subnet.public-subnet.*.id}"]
        value = ["${aws_subnet.private-subnet.*.id}"]
}

output "instance_id" {
    value = "${aws_instance.webserver.id}"
        value = "${aws_instance.dbserver.id}"
}

output "instance_ip" {
    value = "${aws_instance.webserver.public_ip}"
        value = "${aws_instance.dbserver.public_ip}"
}

