# Define AWS as our provider

provider "aws" {
access_key = "${var.aws_access_key}"
secret_key = "${var.aws_secret_key}"
region = "us-west-1"
}

# Define our VPC

resource "aws_vpc" "default" {
  cidr_block = "${var.vpc_cidr}"

  tags {
    Name = "vpc-origin"
  }
}

# Define the public subnet

resource "aws_subnet" "public-subnet" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "${var.public_cidr}"
  availability_zone = "us-west-1a"

  tags {
    Name = "Web Public Subnet"
  }
}

# Define the private subnet

resource "aws_subnet" "private-subnet" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "${var.private_cidr}"
  availability_zone = "us-west-1b"

  tags {
    Name = "Database Private Subnet"
  }
}

# Define the internet gateway

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.default.id}"

  tags {
    Name = "test-vpc-igw"
  }
}

#Define route table

resource "aws_route_table" "public-rt" {
  vpc_id = "${aws_vpc.default.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  tags {
    Name = "public-rt"
  }
}


#Associate Route table

resource "aws_route_table_association" "public-rt" {
  subnet_id = "${aws_subnet.public-subnet.id}"
  route_table_id = "${aws_route_table.public-rt.id}"
}

#Define security group

resource "aws_security_group" "sg" {
    name = "vpc_tf_sg"
    vpc_id = "${aws_vpc.default.id}"
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.sg_cidr}"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "tcp"
        cidr_blocks = ["${var.sg_cidr}"]
    }
}

#Define webserver instance

resource "aws_instance" "webserver" {
   ami  = "${lookup(var.ami,var.aws_region)}"
   instance_type = "${var.aws_instance_type}"
   key_name = "${var.ec2_keys}"
   subnet_id = "${aws_subnet.public-subnet.id}"
   vpc_security_group_ids = ["${aws_security_group.sg.id}"]
    tags {
         Name = "webserver"
  }
}

#Define dbserver instance

resource "aws_instance" "dbserver" {
   ami  = "${lookup(var.ami,var.aws_region)}"
   instance_type = "${var.aws_instance_type}"
   key_name = "${var.ec2_keys}"
   subnet_id = "${aws_subnet.private-subnet.id}"
   vpc_security_group_ids = ["${aws_security_group.sg.id}"]
    tags {
         Name = "dbserver"
  }
}
